The project has been developed on Java 8.

Clone the project and run mvn clean install command.

Make sure to update database user and password.

http://localhost:8181/swagger-ui/index.html#/ will show the end pints to be used. 
In the project there are some additional files.

openApi.txt - the Open Api specifiaction
ApiSpecification.JPG - a screenshot of the specifiaction
Api-UI.JPG - the endpints to be used