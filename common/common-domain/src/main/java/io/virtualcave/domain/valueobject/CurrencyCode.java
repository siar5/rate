package io.virtualcave.domain.valueobject;

public enum CurrencyCode {
    USD, EUR
}
