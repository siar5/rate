package io.virtualcave.domain.valueobject;

public class RateId extends BaseId<Integer>{
    public RateId(Integer value) {
        super(value);
    }
}
