package io.virtualcave.domain.valueobject;

public class BrandId extends BaseId<Integer>{
    public BrandId(Integer value) {
        super(value);
    }
}
