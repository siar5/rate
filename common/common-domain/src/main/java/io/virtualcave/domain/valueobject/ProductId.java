package io.virtualcave.domain.valueobject;

public class ProductId extends BaseId<Integer>{
    public ProductId(Integer value) {
        super(value);
    }
}
