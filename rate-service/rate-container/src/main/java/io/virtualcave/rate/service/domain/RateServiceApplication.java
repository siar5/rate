package io.virtualcave.rate.service.domain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = { "io.virtualcave.rate.service.dataaccess" })
@EntityScan(basePackages = { "io.virtualcave.rate.service.dataaccess"})
@SpringBootApplication(scanBasePackages = "io.virtualcave")
public class RateServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(RateServiceApplication.class, args);
    }
}
