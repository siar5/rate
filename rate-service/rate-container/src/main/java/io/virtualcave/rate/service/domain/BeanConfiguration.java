package io.virtualcave.rate.service.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public RateDomainService rateDomainService(){
        return new RateDomainServiceImpl();
    }

}
