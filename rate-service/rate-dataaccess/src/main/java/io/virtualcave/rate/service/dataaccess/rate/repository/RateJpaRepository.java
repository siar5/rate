package io.virtualcave.rate.service.dataaccess.rate.repository;

import io.virtualcave.rate.service.dataaccess.rate.entity.RateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RateJpaRepository extends JpaRepository<RateEntity, Integer> {
}
