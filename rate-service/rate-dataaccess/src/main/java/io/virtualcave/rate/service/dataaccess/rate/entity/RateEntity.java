package io.virtualcave.rate.service.dataaccess.rate.entity;

import io.virtualcave.domain.valueobject.CurrencyCode;
import lombok.*;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "RATES")
@Entity
public class RateEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer brandId;
    private Integer productId;
    //private ZonedDateTime startDate;
    //private ZonedDateTime endDate;
    private LocalDate startDate;
    private LocalDate endDate;
    private BigInteger price;
    @Enumerated(EnumType.STRING)
    private CurrencyCode currencyCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RateEntity that = (RateEntity) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
