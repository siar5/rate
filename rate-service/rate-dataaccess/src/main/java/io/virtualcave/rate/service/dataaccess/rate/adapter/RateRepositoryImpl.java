package io.virtualcave.rate.service.dataaccess.rate.adapter;

import io.virtualcave.domain.valueobject.RateId;
import io.virtualcave.rate.service.dataaccess.rate.mapper.RateDataAccessMapper;
import io.virtualcave.rate.service.dataaccess.rate.repository.RateJpaRepository;
import io.virtualcave.rate.service.domain.entity.Rate;
import io.virtualcave.rate.service.domain.ports.output.repository.RateRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class RateRepositoryImpl implements RateRepository {

    private final RateJpaRepository rateJpaRepository;
    private final RateDataAccessMapper rateDataAccessMapper;

    public RateRepositoryImpl(RateJpaRepository rateJpaRepository, RateDataAccessMapper rateDataAccessMapper) {
        this.rateJpaRepository = rateJpaRepository;
        this.rateDataAccessMapper = rateDataAccessMapper;
    }

    @Override
    public Rate save(Rate rate) {
        return rateDataAccessMapper.rateEntityToRate(rateJpaRepository.save(rateDataAccessMapper.rateToRateEntity(rate)));
    }

    @Override
    public Rate update(Rate rate) {
        return null;
    }

    @Override
    public Optional<Rate> findById(RateId rateId) {
        return rateJpaRepository.findById(rateId.getValue()).map(rateDataAccessMapper::rateEntityToRate);
    }

    @Override
    public void deleteById(RateId rateId) {
        rateJpaRepository.deleteById(rateId.getValue());
    }
}
