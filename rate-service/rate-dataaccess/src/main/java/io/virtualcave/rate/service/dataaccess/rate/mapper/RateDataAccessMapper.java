package io.virtualcave.rate.service.dataaccess.rate.mapper;

import io.virtualcave.domain.valueobject.BrandId;
import io.virtualcave.domain.valueobject.Money;
import io.virtualcave.domain.valueobject.ProductId;
import io.virtualcave.domain.valueobject.RateId;
import io.virtualcave.rate.service.dataaccess.rate.entity.RateEntity;
import io.virtualcave.rate.service.domain.entity.Rate;
import org.springframework.stereotype.Component;

@Component
public class RateDataAccessMapper {

    public RateEntity rateToRateEntity(Rate rate) {
        RateEntity rateEntity = RateEntity.builder()
                //.id(3)
                .brandId(rate.getBrandId().getValue())
                .productId(rate.getProductId().getValue())
                .startDate(rate.getStartDate())
                .endDate(rate.getEndDate())
                .price(rate.getPrice().getAmount())
                .currencyCode(rate.getCurrencyCode())
                .build();

        return rateEntity;
    }

    public Rate rateEntityToRate(RateEntity rateEntity){
        Rate rate = Rate.Builder.builder()
                .rateId(new RateId(rateEntity.getId()))
                .brandId(new BrandId(rateEntity.getBrandId()))
                .productId(new ProductId(rateEntity.getProductId()))
                .startDate(rateEntity.getStartDate())
                .endDate(rateEntity.getEndDate())
                .price(new Money(rateEntity.getPrice()))
                .currencyCode(rateEntity.getCurrencyCode())
                .build();

        return rate;
    }
}
