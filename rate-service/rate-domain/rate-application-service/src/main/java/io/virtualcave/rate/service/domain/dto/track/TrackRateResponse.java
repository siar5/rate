package io.virtualcave.rate.service.domain.dto.track;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Builder
@AllArgsConstructor
public class TrackRateResponse {

    @NotNull
    private String formattedPrice;
    private List<String> failureMessages;
}
