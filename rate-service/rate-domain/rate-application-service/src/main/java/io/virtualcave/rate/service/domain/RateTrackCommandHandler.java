package io.virtualcave.rate.service.domain;

import io.virtualcave.domain.valueobject.RateId;
import io.virtualcave.rate.service.domain.dto.track.TrackRateQuery;
import io.virtualcave.rate.service.domain.dto.track.TrackRateResponse;
import io.virtualcave.rate.service.domain.entity.Rate;
import io.virtualcave.rate.service.domain.exception.RateDomainException;
import io.virtualcave.rate.service.domain.mapper.RateDataMapper;
import io.virtualcave.rate.service.domain.ports.output.repository.RateRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Component
public class RateTrackCommandHandler {

    private final RateDataMapper rateDataMapper;
    private final RateRepository rateRepository;
    //Here we should have a repository to be able to extract all the currency types.

    public RateTrackCommandHandler(RateDataMapper rateDataMapper, RateRepository rateRepository) {
        this.rateDataMapper = rateDataMapper;
        this.rateRepository = rateRepository;
    }

    @Transactional(readOnly = true)
    public TrackRateResponse trackRate(TrackRateQuery trackRateQuery){
        Optional<Rate> rateResult = rateRepository.findById(new RateId(trackRateQuery.getRateId()));
        if(!rateResult.isPresent()){
            log.warn("Rate not found");
            throw new RateDomainException("Rate not found");
        }

        return rateDataMapper.rateToTrackRateResponse(rateResult.get());
    }
}
