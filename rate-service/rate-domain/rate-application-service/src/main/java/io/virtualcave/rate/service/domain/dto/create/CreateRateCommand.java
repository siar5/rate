package io.virtualcave.rate.service.domain.dto.create;

import io.virtualcave.domain.valueobject.BrandId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.time.LocalDate;

@Getter
@Builder
@AllArgsConstructor
public class CreateRateCommand {

    //private final Integer rateId;
    @NotNull
    private final Integer brandId;
    @NotNull
    private final Integer productId;
    @NotNull
    private final BigInteger price;
    @NotNull
    private final LocalDate startDate;
    @NotNull
    private final LocalDate endDate;
    @NotNull
    private final String currencyCode;

}
