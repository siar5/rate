package io.virtualcave.rate.service.domain.dto.track;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class TrackRateQuery {

    private final Integer rateId;
}
