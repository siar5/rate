package io.virtualcave.rate.service.domain.mapper;

import io.virtualcave.domain.valueobject.*;
import io.virtualcave.rate.service.domain.dto.create.CreateRateCommand;
import io.virtualcave.rate.service.domain.dto.create.CreateRateResponse;
import io.virtualcave.rate.service.domain.dto.track.TrackRateResponse;
import io.virtualcave.rate.service.domain.entity.Rate;
import org.springframework.stereotype.Component;

@Component
public class RateDataMapper {

    public Rate createRateCommandToRate(CreateRateCommand createRateCommand){
        return Rate.Builder.builder()
                .brandId(new BrandId(createRateCommand.getBrandId()))
                .productId(new ProductId(createRateCommand.getProductId()))
                .startDate(createRateCommand.getStartDate())
                .endDate(createRateCommand.getEndDate())
                .price(new Money(createRateCommand.getPrice()))
                .currencyCode(CurrencyCode.valueOf(createRateCommand.getCurrencyCode()))
                .build();
    }

    public CreateRateResponse rateToCreateRateResponse(Rate rate, String message){
        RateId  rateId = (RateId) rate.getId();
        return CreateRateResponse.builder()
                .rateId(rateId!=null ? rateId.getValue() : 0)
                .message(message)
                .build();
    }

    public TrackRateResponse rateToTrackRateResponse(Rate rate) {
        return TrackRateResponse.builder()
                .formattedPrice(rate.getPrice().getAmount() + "-" + rate.getCurrencyCode().toString())
                .build();
    }
}
