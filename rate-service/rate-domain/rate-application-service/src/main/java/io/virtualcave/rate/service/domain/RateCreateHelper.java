package io.virtualcave.rate.service.domain;

import io.virtualcave.rate.service.domain.dto.create.CreateRateCommand;
import io.virtualcave.rate.service.domain.entity.Rate;
import io.virtualcave.rate.service.domain.event.RateCreatedEvent;
import io.virtualcave.rate.service.domain.exception.RateDomainException;
import io.virtualcave.rate.service.domain.mapper.RateDataMapper;
import io.virtualcave.rate.service.domain.ports.output.repository.RateRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class RateCreateHelper {

    private final RateDomainService rateDomainService;
    private final RateRepository rateRepository;
    private final RateDataMapper rateDataMapper;

    public RateCreateHelper(RateDomainService rateDomainService, RateRepository rateRepository, RateDataMapper rateDataMapper) {
        this.rateDomainService = rateDomainService;
        this.rateRepository = rateRepository;
        this.rateDataMapper = rateDataMapper;
    }

    @Transactional
    public RateCreatedEvent persistOrder(CreateRateCommand createRateCommand) {
        //chackProduct using Product service. If not exist, throw new RateDomainException.
        //chackBrand using Brand service. If not exist, throw new RateDomainException.

        //To continue, we assume that Produtc and Brand exist
        Rate rate = rateDataMapper.createRateCommandToRate(createRateCommand);

        RateCreatedEvent rateCreatedEvent = rateDomainService.createRate(rate);
        rate = saveRate(rate);
        rateCreatedEvent = rateDomainService.createRate(rate); //The id should not be autoincrement. It makes the unit test difficult.


        log.info("Rate is created");
        return rateCreatedEvent;
    }

    private Rate saveRate(Rate rate) {
        Rate rateResult = rateRepository.save(rate);

        if(rateResult==null){
            log.error("Could not save order");
            throw new RateDomainException("Could not save order");
        }
        log.info("Rate is saved");
        return rateResult;
    }
}
