package io.virtualcave.rate.service.domain.ports.input.service;

import io.virtualcave.rate.service.domain.dto.create.CreateRateCommand;
import io.virtualcave.rate.service.domain.dto.create.CreateRateResponse;
import io.virtualcave.rate.service.domain.dto.delete.DeleteRateCommand;
import io.virtualcave.rate.service.domain.dto.track.TrackRateQuery;
import io.virtualcave.rate.service.domain.dto.track.TrackRateResponse;

import javax.validation.Valid;

public interface RateApplicationService {

    CreateRateResponse createRate(@Valid CreateRateCommand createRateCommand);

    TrackRateResponse trackRate(@Valid TrackRateQuery trackRateQuery);

    void deleteRate(@Valid DeleteRateCommand deleteRateCommand);

}
