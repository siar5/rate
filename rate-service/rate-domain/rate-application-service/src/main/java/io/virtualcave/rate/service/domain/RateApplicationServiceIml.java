package io.virtualcave.rate.service.domain;

import io.virtualcave.rate.service.domain.dto.create.CreateRateCommand;
import io.virtualcave.rate.service.domain.dto.create.CreateRateResponse;
import io.virtualcave.rate.service.domain.dto.delete.DeleteRateCommand;
import io.virtualcave.rate.service.domain.dto.track.TrackRateQuery;
import io.virtualcave.rate.service.domain.dto.track.TrackRateResponse;
import io.virtualcave.rate.service.domain.ports.input.service.RateApplicationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Slf4j
@Validated
@Service
class RateApplicationServiceIml implements RateApplicationService {

    private final RateCreateCommandHandler rateCreateCommandHandler;
    private final RateTrackCommandHandler rateTrackCommandHandler;
    private final RateDeleteCommandHandler rateDeleteCommandHandler;

    public RateApplicationServiceIml(RateCreateCommandHandler rateCreateCommandHandler, RateTrackCommandHandler rateTrackCommandHandler, RateDeleteCommandHandler rateDeleteCommandHandler) {
        this.rateCreateCommandHandler = rateCreateCommandHandler;
        this.rateTrackCommandHandler = rateTrackCommandHandler;
        this.rateDeleteCommandHandler = rateDeleteCommandHandler;
    }

    @Override
    public CreateRateResponse createRate(CreateRateCommand createRateCommand) {
        return rateCreateCommandHandler.createRate(createRateCommand);
    }

    @Override
    public TrackRateResponse trackRate(TrackRateQuery trackRateQuery) {
        return rateTrackCommandHandler.trackRate(trackRateQuery);
    }

    @Override
    public void deleteRate(DeleteRateCommand deleteRateCommand) {
        rateDeleteCommandHandler.deleteRate(deleteRateCommand);
    }


}
