package io.virtualcave.rate.service.domain;

import io.virtualcave.domain.valueobject.RateId;
import io.virtualcave.rate.service.domain.dto.delete.DeleteRateCommand;
import io.virtualcave.rate.service.domain.entity.Rate;
import io.virtualcave.rate.service.domain.exception.RateDomainException;
import io.virtualcave.rate.service.domain.ports.output.repository.RateRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Component
public class RateDeleteCommandHandler {

    private final RateRepository rateRepository;

    public RateDeleteCommandHandler(RateRepository rateRepository) {
        this.rateRepository = rateRepository;
    }

    @Transactional
    public void deleteRate(DeleteRateCommand deleteRateCommand){
        Optional<Rate> rateResult = rateRepository.findById(new RateId(deleteRateCommand.getRateId()));
        if(!rateResult.isPresent()){
            log.warn("Rate not found");
            throw new RateDomainException("Rate not found");
        } else {
            rateRepository.deleteById(new RateId(deleteRateCommand.getRateId()));
        }
    }
}
