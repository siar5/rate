package io.virtualcave.rate.service.domain.dto.create;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class CreateRateResponse {

    private Integer rateId;
    private String message;
}
