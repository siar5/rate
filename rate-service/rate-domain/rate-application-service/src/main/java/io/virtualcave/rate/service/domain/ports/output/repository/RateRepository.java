package io.virtualcave.rate.service.domain.ports.output.repository;

import io.virtualcave.domain.valueobject.RateId;
import io.virtualcave.rate.service.domain.entity.Rate;

import java.util.Optional;

public interface RateRepository {

    Rate save(Rate rate);

    Rate update(Rate rate);

    Optional<Rate> findById(RateId rateId);

    void deleteById(RateId rateId);
}
