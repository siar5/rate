package io.virtualcave.rate.service.domain;

import io.virtualcave.rate.service.domain.dto.create.CreateRateCommand;
import io.virtualcave.rate.service.domain.dto.create.CreateRateResponse;
import io.virtualcave.rate.service.domain.entity.Rate;
import io.virtualcave.rate.service.domain.event.RateCreatedEvent;
import io.virtualcave.rate.service.domain.exception.RateDomainException;
import io.virtualcave.rate.service.domain.mapper.RateDataMapper;
import io.virtualcave.rate.service.domain.ports.output.repository.RateRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class RateCreateCommandHandler {

    private final RateCreateHelper rateCreateHelper;
    private final RateDataMapper rateDataMapper;

    public RateCreateCommandHandler(RateCreateHelper rateCreateHelper, RateDataMapper rateDataMapper) {
        this.rateCreateHelper = rateCreateHelper;
        this.rateDataMapper = rateDataMapper;
    }

    @Transactional
    public CreateRateResponse createRate(CreateRateCommand createRateCommand){

        RateCreatedEvent rateCreatedEvent = rateCreateHelper.persistOrder(createRateCommand);
        log.info("Rate is created");
        return rateDataMapper.rateToCreateRateResponse(rateCreatedEvent.getRate(), "Rate is created");
    }
}
