package io.virtualcave.rate.service.domain;

import io.virtualcave.rate.service.domain.ports.output.repository.RateRepository;
import org.mockito.Mockito;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages = "io.virtualcave")
public class RateTestConfiguration {

    @Bean
    public RateRepository rateRepository(){
        return Mockito.mock(RateRepository.class);
    }

    @Bean
    public RateDomainService rateDomainService() {
        return new RateDomainServiceImpl();
    }
}
