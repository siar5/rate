package io.virtualcave.rate.service.domain;

import io.virtualcave.rate.service.domain.dto.create.CreateRateCommand;
import io.virtualcave.rate.service.domain.dto.create.CreateRateResponse;
import io.virtualcave.rate.service.domain.entity.Rate;
import io.virtualcave.rate.service.domain.exception.RateDomainException;
import io.virtualcave.rate.service.domain.mapper.RateDataMapper;
import io.virtualcave.rate.service.domain.ports.input.service.RateApplicationService;
import io.virtualcave.rate.service.domain.ports.output.repository.RateRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;
import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(classes = RateTestConfiguration.class)
public class RateApplicationServiceTest {

    @Autowired
    private RateApplicationService rateApplicationService;

    @Autowired
    private RateDataMapper rateDataMapper;

    @Autowired
    private RateRepository rateRepository;

    private CreateRateCommand createRateCommand;
    private CreateRateCommand createRateCommandWrongDate;
    private CreateRateCommand createRateCommandWrongPrice;
    private final Integer productId = 1;
    private final Integer brandId = 1;
    private final BigInteger price = new BigInteger("200");
    private final LocalDate startDate = LocalDate.parse("2022-05-28");
    private final LocalDate endDate = LocalDate.parse("2022-05-29");
    private final String currencyCode = "USD";

    private final BigInteger wrongPrice = new BigInteger("0");

    @BeforeAll
    public void init(){
        createRateCommand = CreateRateCommand.builder()
                .brandId(brandId)
                .productId(productId)
                .price(price)
                .startDate(startDate)
                .endDate(endDate)
                .currencyCode(currencyCode)
                .build();

        createRateCommandWrongDate = CreateRateCommand.builder()
                .brandId(brandId)
                .productId(productId)
                .price(price)
                .startDate(endDate)
                .endDate(startDate)
                .currencyCode(currencyCode)
                .build();

        createRateCommandWrongPrice = CreateRateCommand.builder()
                .brandId(brandId)
                .productId(productId)
                .price(wrongPrice)
                .startDate(startDate)
                .endDate(endDate)
                .currencyCode(currencyCode)
                .build();

        Rate rate = rateDataMapper.createRateCommandToRate(createRateCommand);

        when(rateRepository.save(any(Rate.class))).thenReturn(rate);
    }

    @Test
    public void testCreateRate() {
        CreateRateResponse createRateResponse = rateApplicationService.createRate(createRateCommand);
        assertEquals("Rate is created", createRateResponse.getMessage());
    }

    @Test
    public void testCreateRateWithWrongDate() {
        RateDomainException rateDomainException = assertThrows(RateDomainException.class,
                () -> rateApplicationService.createRate(createRateCommandWrongDate));
        assertEquals("The start date is greater than the end date", rateDomainException.getMessage());
    }

    @Test
    public void testCreateRateWithWrongPrice() {
        RateDomainException rateDomainException = assertThrows(RateDomainException.class,
                () -> rateApplicationService.createRate(createRateCommandWrongPrice));
        assertEquals("The price must be greater than 0", rateDomainException.getMessage());
    }


}
