package io.virtualcave.rate.service.domain;

import io.virtualcave.rate.service.domain.entity.Rate;
import io.virtualcave.rate.service.domain.event.RateCreatedEvent;
import io.virtualcave.rate.service.domain.event.RateDeletedEvent;
import io.virtualcave.rate.service.domain.event.RateUpdatedEvent;
import lombok.extern.slf4j.Slf4j;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@Slf4j
public class RateDomainServiceImpl implements RateDomainService{

    private static final String UTC = "UTC";

    @Override
    public RateCreatedEvent createRate(Rate rate) {
        rate.validateRate(); //The validation could be different for each operation
        log.info("Rate is validated to be created");
        return new RateCreatedEvent(rate, ZonedDateTime.now(ZoneId.of(UTC)));
    }

    @Override
    public RateUpdatedEvent updateRate(Rate rate) {
        rate.validateRate(); //The validation could be different for each operation
        log.info("Rate is validated to be updated");
        return new RateUpdatedEvent(rate, ZonedDateTime.now(ZoneId.of(UTC))) ;
    }

    @Override
    public RateDeletedEvent deleteRate(Rate rate) {
        rate.validateRate(); //The validation could be different for each operation
        log.info("Rate is validated to be deleted");
        return new RateDeletedEvent(rate, ZonedDateTime.now(ZoneId.of(UTC)));
    }
}
