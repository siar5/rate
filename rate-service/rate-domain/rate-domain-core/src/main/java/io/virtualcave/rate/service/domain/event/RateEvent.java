package io.virtualcave.rate.service.domain.event;

import io.virtualcave.domain.event.DomainEvent;
import io.virtualcave.rate.service.domain.entity.Rate;

import java.time.ZonedDateTime;

public abstract class RateEvent implements DomainEvent {

    private final Rate rate;
    private final ZonedDateTime createdAt;

    public RateEvent(Rate rate, ZonedDateTime createdAt) {
        this.rate = rate;
        this.createdAt = createdAt;
    }

    public Rate getRate() {
        return rate;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }
}
