package io.virtualcave.rate.service.domain.event;

import io.virtualcave.rate.service.domain.entity.Rate;

import java.time.ZonedDateTime;

public class RateUpdatedEvent extends RateEvent {

    public RateUpdatedEvent(Rate rate, ZonedDateTime createdAt) {
        super(rate, createdAt);
    }
}
