package io.virtualcave.rate.service.domain.exception;

import io.virtualcave.domain.exception.DomainException;

public class RateNotFoundException extends DomainException {
    public RateNotFoundException(String message) {
        super(message);
    }

    public RateNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
