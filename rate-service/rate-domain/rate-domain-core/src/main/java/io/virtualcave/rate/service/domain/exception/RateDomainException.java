package io.virtualcave.rate.service.domain.exception;

import io.virtualcave.domain.exception.DomainException;

public class RateDomainException extends DomainException {
    public RateDomainException(String message) {
        super(message);
    }

    public RateDomainException(String message, Throwable cause) {
        super(message, cause);
    }
}
