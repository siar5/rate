package io.virtualcave.rate.service.domain.event;

import io.virtualcave.rate.service.domain.entity.Rate;

import java.time.ZonedDateTime;

public class RateDeletedEvent extends RateEvent{
    public RateDeletedEvent(Rate rate, ZonedDateTime createdAt) {
        super(rate, createdAt);
    }
}
