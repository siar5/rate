package io.virtualcave.rate.service.domain.entity;

import io.virtualcave.domain.entity.AggregateRoot;
import io.virtualcave.domain.valueobject.*;
import io.virtualcave.rate.service.domain.exception.RateDomainException;

import java.time.LocalDate;

public class Rate extends AggregateRoot<RateId> {
    private final BrandId brandId;
    private final ProductId productId;
    private final Money price;
    private final LocalDate startDate;
    private final LocalDate endDate;
    private final CurrencyCode currencyCode;


    public void validateRate() {
        validateDates();
        validatePrice();
        validateProduct();
        validateBrand();
    }

    private void validateBrand() {
        if(brandId==null){
            throw new RateDomainException("The brand is not specified.");
        }
    }

    private void validateProduct() {
        if(productId==null){
            throw new RateDomainException("The product is not specified.");
        }
    }

    private void validatePrice() {
        if(price==null || !price.isGreaterThanZero()){
            throw new RateDomainException("The price must be greater than 0");
        }
    }

    private void validateDates() {
        if(startDate!=null && endDate!=null){
            int result = startDate.compareTo(endDate);
            if (result > 0) {
                throw new RateDomainException("The start date is greater than the end date");
            }
        } else {
            throw new RateDomainException("Start and end dates must not be null");
        }
    }


    private Rate(Builder builder) {
        setId(builder.rateId);
        brandId = builder.brandId;
        productId = builder.productId;
        price = builder.price;
        startDate = builder.startDate;
        endDate = builder.endDate;
        currencyCode = builder.currencyCode;
    }


    public BrandId getBrandId() {
        return brandId;
    }

    public ProductId getProductId() {
        return productId;
    }

    public Money getPrice() {
        return price;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }

    public static final class Builder {
        private RateId rateId;
        private BrandId brandId;
        private ProductId productId;
        private Money price;
        private LocalDate startDate;
        private LocalDate endDate;
        private CurrencyCode currencyCode;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public Builder rateId(RateId val) {
            rateId = val;
            return this;
        }

        public Builder brandId(BrandId val) {
            brandId = val;
            return this;
        }

        public Builder productId(ProductId val) {
            productId = val;
            return this;
        }

        public Builder price(Money val) {
            price = val;
            return this;
        }

        public Builder startDate(LocalDate val) {
            startDate = val;
            return this;
        }

        public Builder endDate(LocalDate val) {
            endDate = val;
            return this;
        }

        public Builder currencyCode(CurrencyCode val) {
            currencyCode = val;
            return this;
        }

        public Rate build() {
            return new Rate(this);
        }
    }
}
