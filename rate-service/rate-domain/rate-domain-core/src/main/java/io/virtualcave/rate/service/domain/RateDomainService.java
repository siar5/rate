package io.virtualcave.rate.service.domain;

import io.virtualcave.rate.service.domain.entity.Rate;
import io.virtualcave.rate.service.domain.event.RateCreatedEvent;
import io.virtualcave.rate.service.domain.event.RateDeletedEvent;
import io.virtualcave.rate.service.domain.event.RateUpdatedEvent;

public interface RateDomainService {

    RateCreatedEvent createRate(Rate rate);

    RateUpdatedEvent updateRate(Rate rate);

    RateDeletedEvent deleteRate(Rate rate);
}
