package io.virtualcave.rate.service.domain.event;

import io.virtualcave.rate.service.domain.entity.Rate;

import java.time.ZonedDateTime;

public class RateCreatedEvent extends RateEvent {

    public RateCreatedEvent(Rate rate, ZonedDateTime createdAt) {
        super(rate, createdAt);
    }
}
