package io.virtualcave.rate.service.application.rest;

import io.virtualcave.rate.service.domain.dto.create.CreateRateCommand;
import io.virtualcave.rate.service.domain.dto.create.CreateRateResponse;
import io.virtualcave.rate.service.domain.dto.delete.DeleteRateCommand;
import io.virtualcave.rate.service.domain.dto.track.TrackRateQuery;
import io.virtualcave.rate.service.domain.dto.track.TrackRateResponse;
import io.virtualcave.rate.service.domain.ports.input.service.RateApplicationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/rates", produces = "application/vnd.api.v1+json")
public class RateController {

    private final RateApplicationService rateApplicationService;

    public RateController(RateApplicationService rateApplicationService) {
        this.rateApplicationService = rateApplicationService;
    }

    @PostMapping
    public ResponseEntity<CreateRateResponse> createRate(@RequestBody CreateRateCommand createRateCommand){
        log.info("Creating rate with createRate controller.");
        CreateRateResponse createRateResponse = rateApplicationService.createRate(createRateCommand);
        log.info(createRateResponse.getMessage());
        return ResponseEntity.ok(createRateResponse);
    }


    @GetMapping("/{rateId}")
    public ResponseEntity<TrackRateResponse> getRateById(@PathVariable int rateId){
        TrackRateResponse trackRateResponse = rateApplicationService.trackRate(TrackRateQuery.builder().rateId(rateId).build());
        log.info("Returned an object with the price: " + trackRateResponse.getFormattedPrice());
        return ResponseEntity.ok(trackRateResponse);
    }

    @DeleteMapping("/{rateId}")
    public ResponseEntity<?> deleteRateById(@PathVariable int rateId) {
        rateApplicationService.deleteRate(DeleteRateCommand.builder().rateId(rateId).build());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
